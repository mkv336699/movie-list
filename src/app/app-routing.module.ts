import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: "", redirectTo: "signin", pathMatch: "full" },
  {
    path: "signin",
    loadChildren: () => import("../app/auth/auth.module").then(m => m.AuthModule),
  },
  {
    path: "movies",
    loadChildren: () => import("../app/home/home.module").then(m => m.HomeModule),
    canActivate: [AuthGuardService]
  },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
