import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl("testuser", [Validators.required]),
    password: new FormControl("v^4!C%CQ94f0", [Validators.required]),
  });

  constructor(
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  submit() {
    const formData = this.loginForm.getRawValue();
    console.log("formData", formData);
    this.authService.login(formData).subscribe((res: any) => {
      console.log("login", res);
      if (res && res.is_success) {
        this._snackBar.open("Login Successfull !", "OK", { duration: 3000, horizontalPosition: "right", verticalPosition: "top" });
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("username", formData.username);
        this.router.navigate(["/movies"]);
      } else {
        this._snackBar.open("Login Failed !", "OK", { duration: 3000, horizontalPosition: "right", verticalPosition: "top" });
      }
    });
  }

}
