import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies/movies.component';
import { HomeRoutingModule } from './home-routing.module';
import { MaterialModule } from '../material.module';
import { HomeComponent } from './home.component';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';



@NgModule({
  declarations: [
    MoviesComponent,
    HomeComponent,
    MovieDetailsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
  ]
})
export class HomeModule { }
