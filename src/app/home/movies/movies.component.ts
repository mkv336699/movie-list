import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MovieService } from 'src/app/services/movie.service';
import { MovieDetailsComponent } from './movie-details/movie-details.component';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

    movies: any;
    pageIndex: number = 1;
    totalMovies: number = 0;
    hasRequestFailed = false;

    constructor(
        private movieService: MovieService,
        private _snackBar: MatSnackBar,
        public dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        this.getData();
    }

    getData() {
        this.movieService.getMovies(this.pageIndex).subscribe((res: any) => {
            console.log("getMovies", res);
            if (res && res.results && res.results.length > 0) {
                this.movies = res.results.map((r: any) => { r.url = "https://ui-avatars.com/api/?name=" + r.title.split(" ").join("+"); return r });
                this.totalMovies = parseInt(res.count);
                console.log("this.movies", this.movies);
                this.hasRequestFailed = false;
            } else {
                this._snackBar.open("Something went wrong. Please try again", "OK", { duration: 3000, horizontalPosition: "right", verticalPosition: "top" });
                this.hasRequestFailed = true;
            }
        });
    }

    pageChanged(eventArgs: any) {
        console.log("pageChanged", eventArgs);
        this.pageIndex = eventArgs.pageIndex + 1;
        this.getData();
    }

    openDialog(movie: any) {
        const dialogRef = this.dialog.open(MovieDetailsComponent, {
            data: movie,
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
}
