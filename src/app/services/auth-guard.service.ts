import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor() { }

  canActivate() {
    const token = localStorage.getItem("token") || null;
    if (token) return true;
    return false;
  }
}
