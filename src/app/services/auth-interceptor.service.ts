import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError, map, of, tap } from 'rxjs';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
    private spinner: NgxSpinnerService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    console.log("yo");

    const authReq = req.clone({
      headers: req.headers.set('Authorization', 'Token ' + localStorage.getItem("token"))
    });

    this.spinner.show();
    return next.handle(authReq).pipe(
      map((e: any) => {
        console.log("map", e);
        if (e.status == 200) {
          this.spinner.hide();
          return e;
        }
      }),
      catchError(err => {
        console.log("err", err);
        this.spinner.hide();
        return err;
      })
    );
  }
}
