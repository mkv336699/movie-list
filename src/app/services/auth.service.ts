import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(payload: any) {
    return this.http.post("https://demo.credy.in/api/v1/usermodule/login/", payload).pipe(
      catchError(err => of(err.error))
    );
  }
}
