import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  
  constructor(
    private http: HttpClient,
  ) { }

  getMovies(pageIndex: number) {
    return this.http.get("https://demo.credy.in/api/v1/maya/movies/?page=" + pageIndex).pipe(
      map((data: any) => {
        console.log("map in service", data);
        data.results.forEach((movie: any) => {
          if (movie.genres == "") {
            console.log("if", movie);
            movie.genres = [];
          }
          else movie.genres = movie.genres.split(",");
        });
        return data;
      }),
      catchError(err => of(err.error))
    );
  }

}
